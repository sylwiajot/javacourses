package SimpleTasks;

public class SongAboutBeer {

    public static void main(String[] args) {

        int x = 0;
        int amount = 99;
        String y = "bottles";

        while (x < amount) {

            System.out.print(amount + " " + y + " of beer on the wall, " + amount + " " + y + " of beer. \n"
                    + "Take one down and pass it around, ");
            amount--;
            if (amount != 1) {
                y = "bottles";
            } else {
                y = "bottle";
            }

            if (amount != 0) {
                System.out.println(amount + " " + y + " of beer on the wall.");
            } else {
                System.out.println("no more " + y + " of beer on the wall.");
            }
            System.out.println();
        }
        System.out.println("No more " + y + " of beer on the wall, no more " + y + " of beer.\n" +
                "Go to the store and buy some more, 99 " + y + " of beer on the wall.");
    }
}

