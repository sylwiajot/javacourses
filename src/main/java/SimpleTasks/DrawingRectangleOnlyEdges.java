package SimpleTasks;

import java.util.Scanner;

public class DrawingRectangleOnlyEdges {

    public static void main(String[] args) {
        int x = 0;
        int y = 0;


        while (true) {
            System.out.println("Let's draw our rectangle. Give us the first length of the side: ");
            try {
                x = validNumber();
                System.out.println("Write the second lenght of the side:");
                y = validNumber();


                for (int i = 0; i < x; i++) {
                    for (int j = 0; j < y; j++)
                        if ((i == 0 && j < y) || (i < x && j == 0) || (i < x && j == y - 1) || i == (x - 1) && j < y) {
                            System.out.print(" * ");
                        } else {
                            System.out.print("   ");
                        }
                    System.out.println();
                }
                System.out.println();
            } catch (NumberFormatException exception) {
                System.out.println("Write correct number");
            }
        }


    }

    public static int validNumber() {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.next();
        if (number.equals("end")) {
            System.exit(0);
        }
        Integer result = Integer.valueOf(number);
        return result;
    }
}