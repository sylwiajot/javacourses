package SimpleTasks;

import sun.util.logging.PlatformLogger;

import java.util.logging.Logger;

public class Permutation {
    public static void main(String[] args) {

        int[] array = {24, 21, 20, 19, 22, 23};
        int temp;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        for (int element : array) {
            System.out.print(element + " ");

        }
        System.out.println();
        isPermutation(array);
    }

    public static int isPermutation(int[] array) {
        for (int j = 0; j < array.length-1; j++) {
            if ((array[j] + 1) != array[j + 1]) {
                System.out.println("0");
                //Logger l = new Logger("logger", Permutation.class.getName());
                //l.log(PlatformLogger.Level.INFO,"0");
                return 0;
            }
        }
        System.out.println("1");
        return 1;
    }
}

