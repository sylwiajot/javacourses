package SimpleTasks;

import java.util.Random;

public class CountingGaps {

    public static void main(String[] args) {
        Random r = new Random();
        int number = r.nextInt(10000);
        //int number = 40;
        System.out.println("Our random number is: " + number);

        String binaryNumber = binaryRecord(number);
        System.out.println("Binary record of our random number: " + binaryNumber.replaceAll("-", ""));

        System.out.println("With - : " + binaryNumber);

        String[] tempArray;
        tempArray = binaryNumber.split("-");

        System.out.println("Our binary Number without -: ");
        for (int i = 0; i < tempArray.length; i++) {
            System.out.println(tempArray[i]);
        }
        System.out.println();


        int maxLenghtOfString = 0;
        int longestString = 0;
        System.out.println("The longest gap in our binary number is: ");
        for (int i = 0; i < tempArray.length; i++) {
            if (tempArray[i].contains("0")) {
                if (tempArray[i].length() > maxLenghtOfString) {
                    maxLenghtOfString = tempArray[i].length();
                    longestString = tempArray[i].length();
                }
            }
        }

       // String [] tempArray2;

        if (tempArray.length <= 2 && tempArray[tempArray.length - 1].endsWith("0")) {
          //  tempArray = new tempArray2[tempArray.length-2];
            System.out.println("0");
        } else {
            System.out.println(longestString - 1);
        }


        /********************************/

        String bin = binaryNumber.replaceAll("-","");
        System.out.println(bin);
        int actualMax = 0;
        int max = 0;
        for (char c : bin.toCharArray()) {
            if (c == '0') {
                actualMax++;
            } else { // 1
                if (actualMax > max) {
                    max = actualMax;
                }
                actualMax = 0;
            }
        }
        System.out.println(max);

        /********************************/
    }

    public static String binaryRecord(int number) {
        String characterString = "";
        while (number > 0) {
            int remainderOfDivision = number % 2;
            if (number % 2 == 0) {
                characterString = remainderOfDivision + characterString;
                number = number / 2;
            } else {
                characterString = "-" + remainderOfDivision + characterString;
                number = number / 2;
            }
        }
        return characterString;
    }
}
