package SimpleTasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.IntStream;

public class TheDifferenceBetweenTwoPartsOfArray {

    public static void main(String[] args) {

        int [] array = {2, 8, 1, 3, 5, 9};
        int result=0;

        ArrayList <Integer> arrayList = new ArrayList<>();

        for (int i=0, p=1; i<array.length-1; i++, p++){
            if (p==1) {
                result = array[i];
            } else {
                result = result + array[i];
            }
            System.out.println("P" + p + ": "+ result + "-" + (IntStream.of(array).sum()-result) + " = " +
                    (Math.abs((result) -  (IntStream.of(array).sum()-result))));

            arrayList.add(Math.abs((result) -  (IntStream.of(array).sum()-result)));
        }

        System.out.println();
        for (int element:arrayList){
            System.out.print(element + " ");

        }
        System.out.println();
        Collections.sort(arrayList);
        System.out.println("Our smallest difference is: " + arrayList.get(0));

    }
}
