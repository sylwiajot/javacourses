package SimpleTasks;

import java.util.Scanner;

public class DrawingRectangle {

    public static void main(String[] args) {
        int side = 0;

        while (true) {
            System.out.println();
            System.out.println("Give the lenght of the side (natural number):");
            try {
                side = checkingNumber();

                for (int i = 0; i < side; i++) {
                    for (int j = 0; j < side; j++)


                        System.out.print("*  ");
                    System.out.println();
                }
            } catch (NumberFormatException exception) {
                System.out.println("It isn't a number, try again");
            }
        }
    }

    public static int checkingNumber() {
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.next();
        if (answer.equals("end")) {
            System.exit(0);
        }
        Integer number = Integer.valueOf(answer);
        return number;
    }

}


