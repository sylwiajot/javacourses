package SimpleTasks;

import java.util.Random;

public class MissingElementInTheArray {


    public static void main(String[] args) {
        Random r = new Random();
        int poczatek = r.nextInt(100); // od 0 do 999
        int rozmiar = 30;
        //int[] array = {21, 24, 23, 20, 22, 26};

        int[] array = new int[rozmiar];
        int indeks = r.nextInt(rozmiar);
        for (int i = 0, j = poczatek; i < array.length; i++, j++) {
            if(i == indeks)
                j++;
            array[i] = j;
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        for (int element : array) {
            System.out.print(element + " ");

        }
        System.out.println();

        for (int j = 0; j < array.length - 1; j++) {
            if (array[j] + 1 != array[j + 1]) {
                System.out.println("Our missing number: " + (array[j]+1));
                break;
            }
        }


    }

}
