package SimpleTasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EvenNumbers {

    public static void main(String[] args) {

        int number;
        while (true) {
            System.out.println("Please write a integer number to check if it's even or odd.");
            try {
                number = getUserInputInteger();
                if (number % 2 == 0) {
                    System.out.println("The number " + number + " is even.");
                    System.out.println();
                } else {
                    System.out.println("The number " + number + " is odd.");
                    System.out.println();
                }
            } catch (InputMismatchException exception) {
                System.out.println("The number is wrong.");
                System.out.println();
            }

        }
    }

    public static int getUserInputInteger() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
