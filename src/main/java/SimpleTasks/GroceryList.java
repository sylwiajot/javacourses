package SimpleTasks;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class GroceryList {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<String> shoppingList = new ArrayList<>();
        int x = 0;
        System.out.println("How many products you want to buy?");
        try {
            int numberOfProducts = scanner.nextInt();

            System.out.println();
            for (int i = 0; i < numberOfProducts; i++) {
                System.out.println("Please write the product:");
                String product = scanner.next();
                System.out.println("You bought " + product + " it's your " + (x + 1) + " item.");
                shoppingList.add(product);
                x++;
            }
            System.out.print("Today you bought: ");
            for (int i = 0; i < shoppingList.size(); i++) {
                if (i == shoppingList.size() - 1)
                    System.out.print(shoppingList.get(i));
                else
                    System.out.print(shoppingList.get(i) + ", ");
            }
        } catch (InputMismatchException exception) {
            System.out.println("It's not a number.");
        }
    }

}