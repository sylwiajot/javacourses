package SimpleTasks;

import java.util.Scanner;

public class BankAccount {


    public static void main(String[] args) throws java.lang.Exception {
        Scanner scanner = new Scanner(System.in);
        String operation = "";
        double total = 0;
        double accountBalance = 0;
        double profit = 0;

        do {
            System.out.println("What kind of operation would you like to do? You can choose 'deposit', 'take out' " +
                    "'account balance'. Press 'the end' when you want to stop.");
            try {
                operation = scanner.nextLine();

                switch (operation) {
                    case "deposit":
                        System.out.println("How much would you like to deposit?");
                        double x = Double.parseDouble(scanner.nextLine());
                        if (x > 0) {
                            System.out.printf("You've deposited %.2f zł. ", x);
                            total = total + 0.99 * x;
                            accountBalance = total;
                            profit = profit + 0.01 * x;

                        } else if (x < 0) {
                            System.out.println("You can't deposit any money in this way. Try again.");
                        }
                        break;

                    case "take out":
                        System.out.println("How much would you like to take out?");
                        x = Double.parseDouble(scanner.nextLine());
                        if (x < accountBalance) {
                            System.out.printf("You took out %.2f zł. ", x);
                            total = total - x - 0.01 * x;
                            accountBalance = total;
                            profit = profit + 0.01 * x;
                        } else if (x > accountBalance) {
                            System.out.println("We're sorry. You are too poor :(");
                        }
                        break;

                    case "account balance":
                        System.out.printf("Your account balance is: %.2f. ", accountBalance);
                        break;

                    case "the end":
                        break;

                    default:
                        System.out.println("This operation can't be realised.");
                        break;

                }
            } catch (NumberFormatException exception) {
                System.out.println("You didn't write correct number. Try again.");
            }
        }
        while (!operation.equals("the end"));
        System.out.printf("Thank you for using our services. Our profit is: %.2f. zł.", profit);

    }
}
