package SimpleTasks;

import java.util.Scanner;

public class AttentionOfTheBomb {

    public static void main(String[] args) {

        System.out.println("Enter a good password:");
        Scanner scanner = new Scanner(System.in);
        int countdawn = scanner.nextInt();
        System.out.println("Error!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        int x = 0;
        while (x < countdawn) {
            System.out.println("The bomb will explode in... " + countdawn + " seconds!");
            countdawn--;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        System.out.println("Escape! Boom!");


    }
}
