package SimpleTasks;

import java.util.Scanner;

public class ReverseASentence {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a sentence to reverse.");
        String sentence = " ";

        while (true) {
            sentence = scanner.nextLine();
            if (sentence.isEmpty()) {
                System.out.println("You should write a sentence. Try again");
            } else if (sentence.equals("end")){
                System.out.println("See you soon");
                System.exit(0);
            }

            String[] ourFirstSentence = sentence.split(" ");
            if (ourFirstSentence.length < 2) {
                System.out.println("You should write minimum 2 words. Try again.");
                continue;
            }

            String[] newSentenceAfterReverse = new String[ourFirstSentence.length];

            for (int i = 0; i < ourFirstSentence.length; i++) {
                newSentenceAfterReverse[i] = ourFirstSentence[ourFirstSentence.length - 1 - i];
            }
            for (String element : newSentenceAfterReverse) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}



