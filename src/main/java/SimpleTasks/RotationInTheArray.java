package SimpleTasks;


import java.util.Random;

public class RotationInTheArray {


    public static void main(String[] args) {
        int amount = 5;
        int change = 2;
        Random r = new Random();
        int[] array = new int[amount];

        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(50);
        }
        for (int element : array) {
            System.out.print(element + " ");

        }

        int x = 0;

        while (x < change) {
            int tmpFromTheEnd = array[array.length - 1];
            for (int i = 0; i < array.length; i++) {
                int tmpFromBegining = array[i];
                array[i] = tmpFromTheEnd;
                tmpFromTheEnd = tmpFromBegining;
            }
            System.out.println();
            x++;
            System.out.println("After " + x + " change:");
            for (int element : array) {
                System.out.print(element + " ");


            }
        }

    }
}
