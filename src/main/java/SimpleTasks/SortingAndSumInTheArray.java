package SimpleTasks;


import java.util.Random;

public class SortingAndSumInTheArray {

    public static void main(String[] args) {

        int zmienna = 20;

        System.out.println("Our unmodified aray:");

        Random rr = new Random();

        Integer[] ourArray = new Integer[50];
        for (int i = 0; i < 50; i++) {
            ourArray[i] = rr.nextInt(100);
        }

        for (int element : ourArray) {
            System.out.print(element + " ");
        }
        for (int i = 0; i < ourArray.length; i++) {
            for (int j = 0; j < ourArray.length - 1; j++) {
                if (ourArray[j] > ourArray[j + 1]) {
                    int tmp = ourArray[j];
                    ourArray[j] = ourArray[j + 1];
                    ourArray[j + 1] = tmp;

                }
            }
        }
        System.out.println();
        System.out.println("Our sorted array");
        for (int element : ourArray) {
            System.out.print(element + " ");
        }


        System.out.println();

        int l = 0;
        int r = ourArray.length - 1;
        System.out.println("Pairs which sum is lower than " + zmienna);
        while (l < r) {
            if ((ourArray[l] + ourArray[r]) < zmienna) {
               for (int i = l; i <= r; i++) {
                    System.out.println(ourArray[l] + " + " + ourArray[i] + " = " + (ourArray[l] + ourArray[i]) );
               }
                l++;
            } else {
                r--;
            }
        }

// the second way of solution but not as effectivie as the first one
//        int k = 1;
//        for (int i = 0; i < ourArray.length; i++) {
//            for (int j = 0; j < ourArray.length - 1; j++) {
//                if (ourArray[i] > zmienna || ourArray[j] > zmienna) {
//                    break;
//                }
//                if ((ourArray[i] + ourArray[j]) < zmienna) {
//                    System.out.println(k + " pair: " + ourArray[i] + " + " + ourArray[j] + " = " +
//                            (ourArray[i] + ourArray[j]));
//                    k++;
//                }
//            }
//        }

        System.out.println();
    }
}
