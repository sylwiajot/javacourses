package SimpleTasks;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class GeneratorOfSentences {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String numberOfSentences = "";
        while (!numberOfSentences.equals("stop")) {
            System.out.println("Write how many sentences you want to generate.");
            try {
                numberOfSentences = scanner.next();
                if (numberOfSentences.equals("stop")) {
                    System.out.println("Thank you for your attention.");
                    System.exit(0);
                }
                int numberOfSentences1 = Integer.parseInt(numberOfSentences);
                int x = 0;
                int i = 1;

                String[] table1 = {"The innovation", "The improvement", "This possibility", "This change", "The optimization"};
                String[] table2 = {"will speed up", "will facilitate", "will rectify", "will enable", "will enhance"};
                String[] table3 = {"process", "access", "conncection", "options", "opportunities"};


                while (x < numberOfSentences1) {
                    Random random = new Random();
                    int randomWorld1 = random.nextInt(table1.length);
                    int randomWorld2 = random.nextInt(table2.length);
                    int randomWorld3 = random.nextInt(table3.length);
                    System.out.println(i + ". " + table1[randomWorld1] + " " + table2[randomWorld2] + " "
                            + table3[randomWorld3] + ".");
                    x++;
                    i++;

                }
            } catch (NumberFormatException exception) {
                System.out.println("It's not correct number.");
            }
            System.out.println();

        }
    }
}
