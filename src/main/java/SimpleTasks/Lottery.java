package SimpleTasks;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Lottery {

    public static void main(String[] args) {

        int x = 1;
        int min = 0;
        int max = 49;

        System.out.println("Let's start our lottery.");
        System.out.println();

        ArrayList<Integer> yourLuckyNumbers = new ArrayList<Integer>(5);
        ArrayList<Integer> theWinningNumbers = new ArrayList<Integer>(5);

        while (x < 7) {
            System.out.println("Give us " + x + " lucky number from 1 to 49!");
            try {
                int luckyNumber = getUserNumber();
                if (checkRange(luckyNumber, min, max)) {
                    if (yourLuckyNumbers.contains(luckyNumber)) {
                        System.out.println("You have just chose the number.");
                        System.out.println();
                    } else {
                        yourLuckyNumbers.add(luckyNumber);
                        x++;
                    }
                } else {
                    System.out.println("Write correct number from the range.");
                    System.out.println();
                }
            } catch (InputMismatchException exception) {
                System.out.println("Funny! Only numbers please :)");
                System.out.println();
            }


        }
        int z = 0;
        while (z < 6) {
            int winningNumber = getRandomIntegerBetweenRange(0, 49);
            theWinningNumbers.add(winningNumber);
            z++;
        }

        System.out.print("Your numbers are: ");
        for (int element : yourLuckyNumbers) {
            System.out.print(element + " ");
        }
        System.out.println();
        System.out.print("The winning lottery numbers today: ");
        for (int element2 : theWinningNumbers) {
            System.out.print(element2 + " ");
        }

        theWinningNumbers.retainAll(yourLuckyNumbers);

        int theSameNumbers = theWinningNumbers.size();
        System.out.println();
        if (theSameNumbers > 1 && theSameNumbers < 5) {
            System.out.println("You have " + theSameNumbers + " the same numbers.");
            if (theSameNumbers < 2) {
                System.out.println("You have " + theSameNumbers + " the same number.");
            }
            for (int element3 : theWinningNumbers) {
                System.out.print(element3 + " ");
            }
        } else {
            System.out.println("Congratulations! You are a millionaire!");
        }

    }

    public static int getUserNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    public static boolean checkRange(int number, int min, int max) {
        return number > min && number < max;
    }


    public static int getRandomIntegerBetweenRange(int min2, int max2) {
        int x = (int) ((Math.random() * ((max2 - min2) + 1)) + min2);
        return x;
    }

}
