package SimpleTasks;

import java.util.Scanner;

public class TheNumberOfCharacters {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write sentence to write (it could contain numbers and special characters.");
        String text = scanner.nextLine();


        char[] arrayOfAllCharacters = text.toCharArray();
        int letter = 0;
        int number = 0;
        int other = 0;
        int space = 0;

        for (int i = 0; i < text.length(); i++) {
            if (Character.isLetter(arrayOfAllCharacters[i])) { // we count letters
                letter++;
            } else if
            (Character.isSpaceChar(arrayOfAllCharacters[i])) {//we count spaces
                space++;
            } else if
            (Character.isDigit(arrayOfAllCharacters[i])) { // we count numbers
                number++;
            } else {
                other++;
            }
        }
        System.out.println("The amount of letters in our text is: " + letter + ", the amount of numbers is: " + number +
                ", the number of spaces is: " + space + " and the number of special charactes is: " + other + ".");
    }
}



