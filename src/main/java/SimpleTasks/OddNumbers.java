package SimpleTasks;


import java.util.ArrayList;
import java.util.Random;

public class OddNumbers {

    public static void main(String[] args) {


        Random r = new Random();
        int size = 9;
        ArrayList<Integer> numbers = new ArrayList<>(size);

        System.out.println("Our random list: ");
        for (int i = 0; i < size; i++) {
            numbers.add(r.nextInt(5));
        }

        ArrayList<Integer> numbers2 = (ArrayList<Integer>)numbers.clone();

        System.out.print(numbers);
        System.out.println();

        for (int i = 0; i < numbers.size(); i++) {
            for (int j = 0; j < numbers.size()-1; j++) {
                if (numbers.get(i) == numbers.get(j+1)) {
                    System.out.println("We removed: " + numbers.get(i) + ", " + numbers.get(j+1));
                    numbers.remove(i);
                    numbers.remove(j);
                    j=0;
                    System.out.println(numbers);
                }
            }
        }
        System.out.println();
        System.out.println("Number(s) which don't have pair: " + numbers);
        System.out.println();

        System.out.print(numbers2);
        System.out.println();

        for (int i = 0; i < numbers2.size()-1; i++) {
            for (int j = i+1; j < numbers2.size(); j++) {
                if (numbers2.get(i) == numbers2.get(j)) {
                    System.out.println("We removed: " + numbers2.get(i) + ", " + numbers2.get(j));
                    numbers2.remove(i);
                    numbers2.remove(j-1); // j jest zawsze po i
                    i--; // zostajemy na tej samej pozycji
                    System.out.println(numbers2);
                    break;
                }
            }
        }
        System.out.println();
        System.out.println("Number(s) which don't have pair: " + numbers2);


    }

}
