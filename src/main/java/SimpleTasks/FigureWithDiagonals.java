package SimpleTasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class FigureWithDiagonals {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please give the lenght of the side:");
        try {
            int side = scanner.nextInt();

            System.out.println("Write if the figure has to have 1 or 2 diagonals.");
            int numberofDiagonals = scanner.nextInt();

            for (int i = 0; i < side; i++) {
                for (int j = 0; j < side; j++)
                    if ((i == 0 && j < side) || (i < side && j == 0) || (i < side && j == side - 1)
                            || (i == (side - 1) && j < side) || i == j || numberofDiagonals == 2 && i == (-j + side - 1)) {
                        System.out.print(" * ");
                    } else {
                        System.out.print("   ");
                    }

                System.out.println();

            }
        } catch (InputMismatchException exception) {
            System.out.println("It's not a correct number.");
        }

    }
}


