package GuessingNumber;

import java.util.Random;

public class GamePlay {

    Random random = new Random();

    public void startAGame() {
        System.out.print("The winner number about I think is... ");
        int randomNumber = random.nextInt(10) + 1;
        System.out.println(randomNumber);
        System.out.println();

        Gamer gamer1 = new Gamer();
        Gamer gamer2 = new Gamer();
        Gamer gamer3 = new Gamer();

        int guess1 = gamer1.guessANumber();
        int guess2 = gamer2.guessANumber();
        int guess3 = gamer3.guessANumber();

        System.out.println("I'm gamer one and I think this is " + guess1);
        System.out.println("I'm gamer two and I think this is " + guess2);
        System.out.println("I'm gamer three and I think this is " + guess3);

        if (guess1 == randomNumber) {
            System.out.println("We have winner and it is gamer one!");
        } else if (guess2 == randomNumber) {
            System.out.println("We have winner and it is gamer second!");
        } else if (guess3 == randomNumber) {
            System.out.println("We have winner and it is gamer three!");
        } else {
            System.out.println("We don't have a winner, let's try again.");
        }
    }
}